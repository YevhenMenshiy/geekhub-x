package org.geekhub.studentregistry;

import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.geekhub.studentregistry.exceptions.ParameterNotSpecifiedException;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.CommonGradeFactory;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.ui.StudentsReader;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import static java.lang.System.in;

@Dependency(appMode = AppModes.MANUAL)
public class CommonStudentsReader implements StudentsReader {

    private static final Logger logger = LoggerFactory.getLogger(CommonStudentsReader.class);
    private Scanner scanner;

    @Override
    public List<Student> readStudents(int studentsCount) {
        List<Student> students = new ArrayList<>();
        logger.log("Please enter " + studentsCount + " students info.");
        read(students, studentsCount);
        logger.log("Reading completed, thanks!");
        return students;
    }

    protected void read(List<Student> students, int studentsCount) {
        try (var scan = new Scanner(in)) {
            scanner = scan;
            int i = 0;
            while (i < studentsCount) {

                try {

                    String name = readAndValidateName(i);
                    Grade grade = readAndValidateGrade(i, new CommonGradeFactory());
                    LocalDateTime date = readAndValidateDate(i);

                    students.add(new Student(name, grade, date));
                    i++;

                } catch (GradeNotSupporedException | InvalidScoreException | ParameterNotSpecifiedException e) {
                    logger.log("Error: " + e.getMessage());
                    logger.log("Please enter " + (i + 1) + " student info again.");
                }
            }
        }
    }

    private LocalDateTime readAndValidateDate(int i) {

        logger.log("Enter " + (i + 1) + " student exam date in YYYY-MM-DD format:");
        LocalDateTime date;

        var dateValue = scanner.nextLine();
        if (dateValue.isBlank()) throw new ParameterNotSpecifiedException("Empty date value is not allowed!");

        logger.log("Enter " + (i + 1) + " student exam time in HH:MM format:");
        var timeValue = scanner.nextLine();
        if (timeValue.isBlank()) throw new ParameterNotSpecifiedException("Empty date value is not allowed!");


        try {
            date = LocalDateTime.parse(dateValue+"T"+timeValue+":00");
        } catch (Exception e) {
            throw new InvalidScoreException("Entered Date is not valid");
        }


        return date;
    }

    protected String readAndValidateName(int i) {

        String name;

        logger.log("Enter " + (i + 1) + " student name:");
        name = scanner.nextLine();
        if (name.isBlank()) throw new ParameterNotSpecifiedException("Empty name is not allowed!");

        return name;
    }

    protected Grade readAndValidateGrade(int i, CommonGradeFactory gradeFactory) {
        logger.log("Enter " + (i + 1) + " student numeric grade:");

        int gradeValue = readAndValidateGradeValue().orElseThrow(RuntimeException::new);

        logger.log("Chose " + (i + 1) + " student grade type:");
        GradeType gradeType = choseGradeType();
        if (gradeType == null) throw new ParameterNotSpecifiedException("Grade Type value is not valid!");

        return gradeFactory.createGrade(gradeType, gradeValue);
    }

    private Optional<Integer> readAndValidateGradeValue() {

        int gradeValue;

        var value = scanner.nextLine();
        if (value.isBlank()) return Optional.empty();

        try {
            gradeValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }

        if (gradeValue < 0 || gradeValue > 100) return Optional.empty();

        return Optional.of(gradeValue);
    }

    private GradeType choseGradeType() {
        GradeType gradeType = null;
        for (int n = 0; n < GradeType.values().length; n++) {
            logger.log("[" + n + "] " + GradeType.values()[n]);
        }

        try {
            if (scanner.hasNextInt()) {
                //added Integer.parseInt because bug, scanner skip name line after nextInt()
                int scannerInput = Integer.parseInt(scanner.nextLine());
                if ((GradeType.values().length - 1) >= scannerInput) gradeType = GradeType.values()[scannerInput];
            } else if (scanner.hasNextLine()) {
                String scannerInput = scanner.nextLine();
                gradeType = GradeType.from(scannerInput);
            }
        } catch (IllegalArgumentException | GradeNotSupporedException e) {
            gradeType = null;
        }

        return gradeType;
    }

}
