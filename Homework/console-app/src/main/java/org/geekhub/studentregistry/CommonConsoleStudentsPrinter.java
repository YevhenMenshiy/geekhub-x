package org.geekhub.studentregistry;

import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.grade.BaseGrade;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.ui.StudentsPrinter;

import java.util.List;

import static java.lang.System.out;

@Dependency
public class CommonConsoleStudentsPrinter implements StudentsPrinter {

    private static final Logger logger = LoggerFactory.getLogger(CommonConsoleStudentsPrinter.class);

    ScoreAnalystPrinter scoreAnalystPrinter;

    public void printStudentsGrade(List<Student> students) {
        BaseGrade grade = (BaseGrade) students.get(0).getGrade();

        for (GradeType gradeType : GradeType.values()) {
            if (gradeType.equals(grade.getGradeType())) {
                logger.log("Students with " + gradeType.name() + " grade");
            }
        }
    }

    public void printStudents(List<Student> students) {
        if (!students.isEmpty()) {
            String leftAlignFormat = "| %-25s | %-9s | %-20s |%n";
            String tableDash = "+---------------------------+-----------+---------------------+%n";

            printStudentsGrade(students);

            out.format(tableDash);
            out.format("| Name                      | Grade     | Date               |%n");
            out.format(tableDash);

            for (Student student : students) {
                int n = student.getName().length() - 25;
                out.format(leftAlignFormat,
                        student.getName() + " ".repeat(Math.max(0, n)),
                        student.getGrade().asPrintVersion(),
                        student.getDate()
                );
            }

            out.format(tableDash);

            scoreAnalystPrinter = new ScoreAnalystPrinter(students);
            scoreAnalystPrinter.printScores();
        }

    }
}
