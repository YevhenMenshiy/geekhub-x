package org.geekhub.studentregistry;

import org.geekhub.studentregistry.grade.CommonGradeFactory;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeFactory;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.services.CommonStudentsAnalyst;
import org.geekhub.studentregistry.services.StudentsAnalyst;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ScoreAnalystPrinter {

    private static final Logger logger = LoggerFactory.getLogger(CommonStudentsReader.class);

    StudentsAnalyst studentsAnalyst;
    List<Student> studentsList;
    GradeFactory gradeFactory;

    public ScoreAnalystPrinter(List<Student> students) {
        studentsAnalyst = new CommonStudentsAnalyst();
        studentsList = students;
        gradeFactory = new CommonGradeFactory();
    }

    public void printScores() {
        printMaxScore();
        printMinScore();
        printMedianScore();
    }

    private void printMaxScore() {
        String maxScore = studentsAnalyst.maxScore(studentsList)
                .map(this::toAllGrades)
                .orElse("N/A");

        logger.log("Max score: " + maxScore);
    }

    private void printMinScore() {
        String minScore = studentsAnalyst.minScore(studentsList)
                .map(this::toAllGrades)
                .orElse("N/A");

        logger.log("Min score: " + minScore);
    }

    private void printMedianScore() {
        String medianScore = studentsAnalyst.medianScore(studentsList)
                .map(this::toAllGrades)
                .orElse("N/A");

        logger.log("Median score:  " + medianScore);
    }

    private String toAllGrades(int score) {
        return Stream.of(GradeType.values())
                .map(grade -> gradeFactory.createGrade(grade, score))
                .map(Grade::asPrintVersion)
                .collect(Collectors.joining(", ", "[","]"));
    }

}
