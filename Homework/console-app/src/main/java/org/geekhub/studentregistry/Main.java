package org.geekhub.studentregistry;

import org.geekhub.studentregistry.exceptions.InvalidStartupArgumentException;

public class Main {

    public static int validateStartArguments(String[] args) {
        if (args.length < 1) {
            throw new InvalidStartupArgumentException("Total students count argument is expected but missing");
        }

        int totalStudentsCount = Integer.parseInt(args[0]);

        if (totalStudentsCount < 1) {
            throw new InvalidStartupArgumentException("Total students count argument is expected to be more then 0");
        }

        return totalStudentsCount;
    }

    public static AppModes applicationMode(String[] args) {
        if (args.length > 1) return AppModes.from(args[1]);
        else throw new InvalidStartupArgumentException("To few startup arguments, app mode is not setup!");
    }

    public static void main(String[] args) {
        AppModes appMode = applicationMode(args);

        int totalStudentsCount = validateStartArguments(args);

        StudentsRegistryFactory studentsRegistryFactory = new StudentsRegistryFactory();

        StudentsRegistry studentsRegistry = studentsRegistryFactory.create(appMode);

        studentsRegistry.run(totalStudentsCount);
    }
}
