package org.geekhub.studentregistry;

import org.geekhub.studentregistry.exceptions.InvalidStartupArgumentException;
import org.geekhub.studentregistry.ui.AutoStudentsReader;
import org.geekhub.studentregistry.ui.StudentsReader;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class MainTest {

        @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void validate_main_null_args_is_not_valid() {
        Main.main(new String[0]);
    }

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void validate_satrtup_null_is_not_valid() {
        Main.validateStartArguments(new String[0]);
    }

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void validate_satrtup_less_1_is_not_valid() {
        String[] args = {"0"};
        Main.validateStartArguments(args);
    }

//    @Test
//    public void ManualResolveStudentReader() {
//        StudentsReader studentsReader = Main.resolveStudentReader(AppModes.MANUAL);
//
//        assertEquals(studentsReader, new CommonStudentsReader());
//    }
//
//    @Test
//    public void AutoResolveStudentReader() {
//        StudentsReader studentsReader = Main.resolveStudentReader(AppModes.AUTO);
//
//        assertEquals(studentsReader, new AutoStudentsReader());
//    }

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void empty_args_application_mode_axaption() {
       Main.applicationMode(new String[0]);
    }

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void length_min_two_args_application_mode_axaption() {
        Main.applicationMode(new String[]{"10"});
    }

    @Test
    public void auto_application_mode() {
        AppModes mode = Main.applicationMode(new String[]{"10", "Auto"});

        assertEquals(mode, AppModes.AUTO);
    }

    @Test
    public void manual_application_mode() {
        AppModes mode = Main.applicationMode(new String[]{"10", "MANUAL"});

        assertEquals(mode, AppModes.from("MANUAL"));
    }

}