package org.geekhub.studentregistry;

import org.geekhub.studentregistry.grade.Grade;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Student implements Serializable {

    private final String name;
    private final Grade grade;
    private final LocalDateTime date;

    public Student(String name, Grade grade, LocalDateTime date) {
        this.name = name;
        this.grade = grade;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public Grade getGrade() {
        return grade;
    }

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        return Objects.equals(name, student.name)
                && Objects.equals(grade, student.grade)
                && Objects.equals(date, student.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, grade, date);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", grade=" + grade +
                ", date=" + date +
                '}';
    }
}
