package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.springframework.stereotype.Repository;

@Repository
public class CommonGradeFactory implements GradeFactory {
    @Override
    public BaseGrade createGrade(GradeType gradeType, int value) {

        if(gradeType == null) throw new GradeNotSupporedException("GradeType is not supported");

        return switch (gradeType) {
            case GPA -> new GpaGrade(value);
            case LETTER -> new LetterGrade(value);
            case PERCENTAGE -> new PercentageGrade(value);
            case UKRAINE -> new UkraineGrade(value);
        };
    }
}
