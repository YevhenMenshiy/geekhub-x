package org.geekhub.studentregistry;

import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.exceptions.InvalidStartupArgumentException;
import org.geekhub.studentregistry.io.StudentsDatabase;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.services.StudentsFilterer;
import org.geekhub.studentregistry.services.StudentsSorter;
import org.geekhub.studentregistry.ui.StudentsPrinter;
import org.geekhub.studentregistry.ui.StudentsReader;
import org.reflections.Reflections;

import java.lang.reflect.InvocationTargetException;
import java.util.Set;
import java.util.stream.Collectors;

public class StudentsRegistryFactory {

    private static final Logger logger = LoggerFactory.getLogger(StudentsRegistryFactory.class);

    public StudentsRegistry create(AppModes appMode) {
        StudentsRegistry studentsRegistry;

        Class<?>[] argTypes = {
                StudentsReader.class, StudentsSorter.class, StudentsFilterer.class, StudentsPrinter.class, StudentsDatabase.class
        };

        Object[] dependencies = getDependencies(appMode, argTypes);

        Object object = createObject(StudentsRegistry.class.getName(), argTypes, dependencies);
        if (object instanceof StudentsRegistry) {
            studentsRegistry = (StudentsRegistry) object;
        } else {
            throw new InvalidStartupArgumentException("Application is not build");
        }

        return studentsRegistry;
    }

    private Object[] getDependencies(AppModes appMode, Class<?>[] argTypes) {

        Reflections reflection = new Reflections("org.geekhub");
        Set<Class<?>> classesWithDependencyAnnotation = reflection.getTypesAnnotatedWith(Dependency.class);
        Set<Class<?>> appModeClassesFilter = classesWithDependencyAnnotation
                .stream()
                .filter(c -> c.getAnnotation(Dependency.class) != null)
                .filter(c -> (c.getAnnotation(Dependency.class).appMode() == appMode || c.getAnnotation(Dependency.class).appMode() == AppModes.NO))
                .collect(Collectors.toSet());

        return sortDependencies(appModeClassesFilter, argTypes);
    }

    private Object[] sortDependencies(Set<Class<?>> classesWithDependencyAnnotation, Class<?>[] argTypes) {
        Object[] sortedDependencies = new Object[argTypes.length];
        int i = 0;
        for (Class<?> c : argTypes) {
            for (Class<?> dependencyClass : classesWithDependencyAnnotation) {
                for (Class<?> interf : dependencyClass.getInterfaces()) {
                    if (c.equals(interf)) {
                        try {
                            sortedDependencies[i++] = dependencyClass.newInstance();
                        } catch (IllegalAccessException | InstantiationException e) {
                            logger.error(e);
                        }
                    }
                }
            }
        }
        return sortedDependencies;
    }

    private Object createObject(String className, Class<?>[] argTypes, Object[] dependencies) {
        Object object = null;
        try {
            object = Class.forName(className).getConstructor(argTypes).newInstance(dependencies);
        } catch (InstantiationException | ClassNotFoundException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            logger.error(e);
        }
        return object;
    }
}
