package org.geekhub.studentregistry.exceptions;

public class readFileException extends RuntimeException {
    public readFileException(String errorMessage) {
        super(errorMessage);
    }
}
