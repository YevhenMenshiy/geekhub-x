package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;

import java.io.Serializable;

public class UkraineGrade extends BaseGrade {

    private static final long serialVersionUID = Long.MAX_VALUE;

    public UkraineGrade(int value) {
        super(value, GradeType.UKRAINE);
    }

    @Override
    public String asPrintVersion() {
        if (getValue() < 5 && getValue() >= 0)
            return "1";
        else if (getValue() < 10 && getValue() >= 5)
            return "2";
        else if (getValue() < 20 && getValue() >= 10)
            return "3";
        else if (getValue() < 30 && getValue() >= 20)
            return "4";
        else if (getValue() < 40 && getValue() >= 30)
            return "5";
        else if (getValue() < 50 && getValue() >= 40)
            return "6";
        else if (getValue() < 60 && getValue() >= 50)
            return "7";
        else if (getValue() < 70 && getValue() >= 60)
            return "8";
        else if (getValue() < 80 && getValue() >= 70)
            return "9";
        else if (getValue() < 90 && getValue() >= 80)
            return "10";
        else if (getValue() < 100 && getValue() >= 90)
            return "11";
        else if (getValue() == 100)
            return "12";
        else throw new InvalidScoreException("Grade value isn`t valid");
    }
}
