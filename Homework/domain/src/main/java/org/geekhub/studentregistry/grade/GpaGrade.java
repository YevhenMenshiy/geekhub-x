package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;

import java.io.Serializable;

public class GpaGrade extends BaseGrade {

    private static final long serialVersionUID = Long.MAX_VALUE;

    public GpaGrade(int value) {
        super(value, GradeType.GPA);
    }

    @Override
    public String asPrintVersion() {
        if (GradeRange.FIRST == getGradeRange())
            return "4.0";
        else if (GradeRange.SECOND == getGradeRange())
            return "3.0";
        else if (GradeRange.THIRD == getGradeRange())
            return "2.0";
        else if (GradeRange.FOURTH == getGradeRange())
            return "1.0";
        else if (GradeRange.FIFTH == getGradeRange())
            return "0.0";
        else throw new InvalidScoreException("Grade value isn`t valid");
    }
}