package org.geekhub.studentregistry.exceptions;

public class ParameterNotSpecifiedException extends RuntimeException {
    public ParameterNotSpecifiedException(String errorMessage) {
        super(errorMessage);
    }
}
