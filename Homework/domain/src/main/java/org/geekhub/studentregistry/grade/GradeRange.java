package org.geekhub.studentregistry.grade;

public enum GradeRange {
    FIRST, SECOND, THIRD, FOURTH, FIFTH
}
