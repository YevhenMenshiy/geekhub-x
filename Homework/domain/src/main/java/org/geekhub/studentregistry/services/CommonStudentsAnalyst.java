package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.BaseGrade;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class CommonStudentsAnalyst implements StudentsAnalyst {
    @Override
    public Optional<Integer> averageScore(List<Student> students) {
        int average = (int) students.stream().map(Student::getGrade)
                .map(grade -> ((BaseGrade) grade).getValue())
                .mapToInt(Integer::intValue)
                .average()
                .orElse(0);

        return Optional.of(average);
    }

    @Override
    public Optional<Integer> maxScore(List<Student> students) {

        int max = students.stream()
                .filter(student -> student.getGrade() instanceof BaseGrade)
                .map(student -> (BaseGrade) student.getGrade())
                .map(BaseGrade::getValue)
                .max(Comparator.comparing(Integer::intValue))
                .orElse(0);

        return Optional.of(max);
    }

    @Override
    public Optional<Integer> minScore(List<Student> students) {
        int min = students.stream()
                .filter(student -> student.getGrade() instanceof BaseGrade)
                .map(student -> (BaseGrade) student.getGrade())
                .map(BaseGrade::getValue)
                .min(Comparator.comparing(Integer::intValue))
                .orElse(0);

        return Optional.of(min);
    }

    @Override
    public Optional<Integer> medianScore(List<Student> students) {

        List<Integer> grades = students.stream()
                .filter(student -> student.getGrade() instanceof BaseGrade)
                .map(student -> (BaseGrade) student.getGrade())
                .map(BaseGrade::getValue)
                .sorted()
                .collect(Collectors.toList());

        if (grades.isEmpty()) {
            return Optional.empty();
        } else {
            long gradesCount = grades.size();
            int medianIndex = (int) (gradesCount / 2);

            return Optional.of(grades.get(medianIndex));
        }
    }
}
