package org.geekhub.studentregistry.exceptions;

public class InvalidStartupArgumentException extends RuntimeException {
    public InvalidStartupArgumentException(String errorMessage) {
        super(errorMessage);
    }
}
