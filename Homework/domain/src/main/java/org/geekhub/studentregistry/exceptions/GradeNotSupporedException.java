package org.geekhub.studentregistry.exceptions;

public class GradeNotSupporedException extends RuntimeException {
    public GradeNotSupporedException(String errorMessage) {
        super(errorMessage);
    }
}
