package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;

public enum GradeType {
    LETTER, PERCENTAGE, GPA, UKRAINE;

    private static final Logger logger = LoggerFactory.getLogger(GradeType.class);

    public static GradeType from(String grade) {
        for (var gradeType : GradeType.values()){
            if(gradeType.name().equalsIgnoreCase(grade)){
                return  gradeType;
            }
        }

        logger.log("GradeType not supported " + grade);
        throw new GradeNotSupporedException("GradeType not supported " + grade);
    }
}
