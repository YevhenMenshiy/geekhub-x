package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;

import java.io.Serializable;
import java.util.Objects;

public abstract class BaseGrade implements Grade, Serializable{

    private static final long serialVersionUID = Long.MAX_VALUE;

    private final int value;
    private final GradeType gradeType;

    public BaseGrade(int value, GradeType gradeType) {

        if (value < 0 || value > 100) throw new InvalidScoreException("Value " + value + " is not valid");

        this.value = value;
        this.gradeType = gradeType;
    }

    public int getValue() {
        return value;
    }

    public GradeType getGradeType() {
        return gradeType;
    }
    
    public GradeRange getGradeRange(){
        if (getValue() >= 90 && getValue() <= 100)
            return GradeRange.FIRST;
        else if (getValue() >= 80 && getValue() < 90)
            return GradeRange.SECOND;
        else if (getValue() >= 70 && getValue() < 80)
            return GradeRange.THIRD;
        else if (getValue() >= 60 && getValue() < 70)
            return GradeRange.FOURTH;
        else if (getValue() >= 0 && getValue() < 60)
            return GradeRange.FIFTH;
        else throw new InvalidScoreException("Grade value isn`t valid");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseGrade baseGrade = (BaseGrade) o;
        return value == baseGrade.value &&
                gradeType == baseGrade.gradeType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, gradeType);
    }

    @Override
    public String toString() {
        return "BaseGrade{" +
                "value=" + value +
                ", gradeType=" + gradeType +
                '}';
    }
}
