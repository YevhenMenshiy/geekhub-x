package org.geekhub.studentregistry.ui;

import org.geekhub.studentregistry.AppModes;
import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.grade.CommonGradeFactory;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeFactory;
import org.geekhub.studentregistry.grade.GradeType;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Dependency(appMode = AppModes.AUTO)
@Component
public class AutoStudentsReader implements StudentsReader {

    String[] names = {
            "Anna",
            "Mary",
            "Tomas",
            "Linda",
            "Andrey",
            "Peter",
            "Natalia",
            "Vladimir",
            "Nikolaus",
            "Karlos",
            "Robert"
    };
    String[] alfabet = {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K"
    };
    Random random = new Random();

    @Override
    public List<Student> readStudents(int studentsCount) {
        List<Student> students = new ArrayList<>();
        makeRandomStudents(students, studentsCount);
        return students;
    }

    private void makeRandomStudents(List<Student> students, int studentsCount) {
        GradeFactory gradeFactory = new CommonGradeFactory();
        for (int i = 0; i < studentsCount; i++) {
            String name = randomName();

            GradeType gradeType = randomGradeType();
            int gradeValue = randomGradeValue();
            Grade grade = gradeFactory.createGrade(gradeType, gradeValue);
            LocalDateTime date = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);

            students.add(new Student(name, grade, date));
        }
    }

    private String randomName() {
        int i = random.nextInt(names.length);
        int j = random.nextInt(alfabet.length);
        return names[i] + " " + alfabet[j] +".";
    }

    private int randomGradeValue() {
        return random.nextInt(100);
    }

    private GradeType randomGradeType() {
        int i = random.nextInt(GradeType.values().length);
        return GradeType.values()[i];
    }
}
