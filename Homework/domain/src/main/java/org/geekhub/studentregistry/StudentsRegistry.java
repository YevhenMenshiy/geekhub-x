package org.geekhub.studentregistry;

import org.geekhub.studentregistry.io.StudentsDatabase;
import org.geekhub.studentregistry.ui.*;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StudentsRegistry {

    private StudentsReader studentsReader;
    private StudentsSorter studentsSorter;
    private StudentsFilterer studentsFilterer;
    private StudentsPrinter studentsPrinter;
    private StudentsDatabase studentsDatabase;

    public StudentsRegistry(
            StudentsReader studentsReader,
            StudentsSorter studentsSorter,
            StudentsFilterer studentsFilterer,
            StudentsPrinter studentsPrinter,
            StudentsDatabase studentsDatabase
    ) {
        this.studentsReader = studentsReader;
        this.studentsSorter = studentsSorter;
        this.studentsFilterer = studentsFilterer;
        this.studentsPrinter = studentsPrinter;
        this.studentsDatabase = studentsDatabase;
    }

    public StudentsRegistry(){}
    @Autowired
    public void setStudentsSorter(StudentsSorter studentsSorter) {
        this.studentsSorter = studentsSorter;
    }
    @Autowired
    public void setStudentsFilterer(StudentsFilterer studentsFilterer) {
        this.studentsFilterer = studentsFilterer;
    }

    public void setStudentsReader(StudentsReader studentsReader) {
        this.studentsReader = studentsReader;
    }
    public void setStudentsPrinter(StudentsPrinter studentsPrinter) {
        this.studentsPrinter = studentsPrinter;
    }
    public void setStudentsDatabase(StudentsDatabase studentsDatabase) { this.studentsDatabase = studentsDatabase; }

    public void run(int totalStudentsCount) {
        List<Student> studentsFromDb;
        studentsFromDb = studentsDatabase.readStudents();

        List<Student> students;
        students = this.studentsReader.readStudents(totalStudentsCount);

        studentsDatabase.writeStudents(students, studentsFromDb);

        students.addAll(studentsFromDb);

        Map<GradeType, List<Student>> studentsByGradeType = new HashMap<>(Collections.emptyMap());
        for (GradeType gradeType : GradeType.values()) {
            List<Student> studentsFiltered = this.studentsFilterer.filterStudentsByGrade(gradeType, students);
            studentsByGradeType.put(gradeType, studentsFiltered);
        }

        studentsByGradeType.forEach((gradeType, filteredStudents) -> {
            List<Student> studentsSorted = this.studentsSorter.sortStudentsByGrade(filteredStudents);
            this.studentsPrinter.printStudents(studentsSorted);
        });

    }
}
