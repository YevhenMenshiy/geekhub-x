package org.geekhub.studentregistry.ui;

import org.geekhub.studentregistry.Student;

import java.util.List;

public interface StudentsReader {

    List<Student> readStudents(int studentsCount);

}