package org.geekhub.studentregistry;

import org.geekhub.studentregistry.exceptions.InvalidStartupArgumentException;

public enum AppModes {
    AUTO, MANUAL, NO;

    public static AppModes from(String mode) {
        for (var appModes : AppModes.values()){
            if(appModes.name().equalsIgnoreCase(mode)){
                return  appModes;
            }
        }

        throw new InvalidStartupArgumentException("Application mode not supported " + mode);
    }
}
