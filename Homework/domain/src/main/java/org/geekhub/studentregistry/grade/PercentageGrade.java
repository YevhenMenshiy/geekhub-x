package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;

import java.io.Serializable;

public class PercentageGrade extends BaseGrade {

    private static final long serialVersionUID = Long.MAX_VALUE;

    public PercentageGrade(int value) {
        super(value, GradeType.PERCENTAGE);
    }

    @Override
    public String asPrintVersion() {
        if (GradeRange.FIRST == getGradeRange())
            return "90%-100%";
        else if (GradeRange.SECOND == getGradeRange())
            return "80%-89%";
        else if (GradeRange.THIRD == getGradeRange())
            return "70%-79%";
        else if (GradeRange.FOURTH == getGradeRange())
            return "60%-69%";
        else if (GradeRange.FIFTH == getGradeRange())
            return "<60%";
        else throw new InvalidScoreException("Grade value isn`t valid");
    }
}
