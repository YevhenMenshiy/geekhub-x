package org.geekhub.studentregistry.exceptions;

public class InvalidScoreException extends RuntimeException {
    public InvalidScoreException(String errorMessage) {
        super(errorMessage);
    }
}
