package org.geekhub.studentregistry.io;

import org.geekhub.studentregistry.Student;

import java.util.List;

public interface StudentsDatabase {

    List<Student> readStudents();

    void writeStudents(List<Student> students, List<Student> studentsFromDb);
}
