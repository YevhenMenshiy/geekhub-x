package org.geekhub.studentregistry.services;

import java.util.*;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.geekhub.studentregistry.exceptions.InvalidInputException;
import org.geekhub.studentregistry.grade.BaseGrade;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Dependency
@Component
public class CommonStudentsFilterer implements StudentsFilterer {
    @Override
    public List<Student> filterStudentsByGrade(GradeType gradeType, List<Student> studentsToFilter) {
        if (null == studentsToFilter) throw new InvalidInputException("Students can`t be null");

            List<Student> studentsFiltered = new ArrayList<>();

            for (Student student : studentsToFilter) {

                Grade studentGrade = student.getGrade();
                if (studentGrade instanceof BaseGrade) {
                    BaseGrade grade = (BaseGrade) student.getGrade();

                    if (grade.getGradeType().equals(gradeType))
                        studentsFiltered.add(student);
                } else {
                    throw new GradeNotSupporedException("Grade mast be instance of BaseGrade class");
                }
            }

        return studentsFiltered;
    }

}
