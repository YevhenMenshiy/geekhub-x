package org.geekhub.studentregistry.annotations;

import org.geekhub.studentregistry.AppModes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Dependency {
    AppModes appMode() default AppModes.NO;
}
