package org.geekhub.studentregistry.io;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.exceptions.readFileException;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

import static org.geekhub.studentregistry.io.FileSplitter.splitFile;

@Dependency
@Component
public class FileStudentsDatabase implements StudentsDatabase {

    private static final Logger logger = LoggerFactory.getLogger(FileStudentsDatabase.class);
    private final String fileName = "Students.db";
    File file;

    public FileStudentsDatabase() {
        file = dbFilePath().toFile();
    }

    private String homeDir() {
        return System.getProperty("user.home");
    }

    private Path dbFilePath() {
        Path path = Path.of(homeDir());
        return path.resolve(fileName);
    }

    private List<Student> readFile() {
        List<Student> students = Collections.emptyList();

        if (file.exists()) {
            try (ObjectInputStream oi = new ObjectInputStream(new FileInputStream(file.getAbsolutePath()))) {
                Object obj = oi.readObject();
                if (obj instanceof List) {
                    students = (List<Student>) obj;
                }
            } catch (IOException | ClassNotFoundException e) {
                logger.error(e);
            }
        }

        return students;
    }

    private void writeToFile(List<Student> students) {

        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file.getAbsolutePath()))) {
            os.writeObject(students);
            os.flush();
        } catch (IOException e) {
            throw new readFileException("Error writing");
        }
    }

    private List getSizeOfStudents(List<Student> students) {
        long fileSizeInBytes = file.length();
        long fileSizeInMB = fileSizeInBytes / (1024*1024);

        if (fileSizeInMB > 1) {
            try {
                return splitFile(file.getAbsolutePath(), 1);
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return students;
    }


    @Override
    public List<Student> readStudents() {
        return readFile();
    }

    @Override
    public void writeStudents(List<Student> students, List<Student> studentsFromDb) {

        writeToFile(students);
        List<Student> studentsSplitBySize = getSizeOfStudents(students);
        writeToFile(studentsSplitBySize);
    }
}
