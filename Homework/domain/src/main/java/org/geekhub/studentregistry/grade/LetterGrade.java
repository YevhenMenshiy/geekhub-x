package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;

import java.io.Serializable;

public class LetterGrade extends BaseGrade {

    private static final long serialVersionUID = Long.MAX_VALUE;

    public LetterGrade(int value) {
        super(value, GradeType.LETTER);
    }

    @Override
    public String asPrintVersion() {
        if (GradeRange.FIRST == getGradeRange())
            return"A";
        else if (GradeRange.SECOND == getGradeRange())
            return"B";
        else if (GradeRange.THIRD == getGradeRange())
            return"C";
        else if (GradeRange.FOURTH == getGradeRange())
            return"D";
        else if (GradeRange.FIFTH == getGradeRange())
            return"F";
        else throw new InvalidScoreException("Grade value isn`t valid");
    }
}
