package org.geekhub.studentregistry.ui;

import org.geekhub.studentregistry.Student;
import java.util.List;

public interface StudentsPrinter {
    void printStudents(List<Student> students);
}
