package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import java.util.List;

public interface StudentsSorter {

    List<Student> sortStudentsByGrade(List<Student> students);

}
