package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.GradeType;

import java.util.List;

public interface StudentsFilterer {
    List<Student> filterStudentsByGrade(GradeType gradeType, List<Student> studentsToFilter);
}
