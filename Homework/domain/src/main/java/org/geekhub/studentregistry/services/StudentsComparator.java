package org.geekhub.studentregistry.services;
import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.BaseGrade;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeRange;

import java.util.Comparator;

public class StudentsComparator implements Comparator<Student> {
    public int compare(Student firstStudent, Student secondStudent) {
        Grade firstStudentGrade = firstStudent.getGrade();
        Grade secondStudentGrade = secondStudent.getGrade();
        int compare = 0;
        if (firstStudentGrade instanceof BaseGrade && secondStudentGrade instanceof BaseGrade) {
            GradeRange firstGradeRange = ((BaseGrade) firstStudentGrade).getGradeRange();
            GradeRange secondGradeRange = ((BaseGrade) secondStudentGrade).getGradeRange();
            compare = Integer.compare(firstGradeRange.ordinal(), secondGradeRange.ordinal());
        }
        if(compare == 0){
            int compareNames = String.CASE_INSENSITIVE_ORDER.compare(firstStudent.getName(), secondStudent.getName());
            return Integer.compare(compareNames, 0);
        }
        return compare;
    }
}
