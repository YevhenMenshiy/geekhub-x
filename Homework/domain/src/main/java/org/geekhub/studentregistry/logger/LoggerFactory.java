package org.geekhub.studentregistry.logger;

public class LoggerFactory {

    public static Logger getLogger(Class c){
        return new Logger(c.getName());
    }

}
