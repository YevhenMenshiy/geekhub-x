package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.annotations.Dependency;
import org.geekhub.studentregistry.exceptions.InvalidInputException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Dependency
@Component
public class CommonStudentsSorter implements StudentsSorter {
    @Override
    public List<Student> sortStudentsByGrade(List<Student> students) {

        if (null == students) throw new InvalidInputException("Students can`t be null");

        Student[] studentsSorted = students.toArray(Student[]::new);

        Arrays.sort(studentsSorted, new StudentsComparator());

        return Arrays.asList(studentsSorted.clone());
    }
}
