package org.geekhub.studentregistry.grade;

public class GradeTestData {

    public static Grade LETTER_GRADE_A = new LetterGrade(95);

    public static Grade LETTER_GRADE_B = new LetterGrade(85);

    public static Grade PERCENTAGE_GRADE = new PercentageGrade(90);

    public static Grade GPA_GRADE = new GpaGrade(90);

}
