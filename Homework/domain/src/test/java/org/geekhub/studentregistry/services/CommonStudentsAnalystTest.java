package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_A;
import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_B;
import static org.testng.Assert.*;

public class CommonStudentsAnalystTest {

    StudentsAnalyst studentsAnalyst;
    private LocalDateTime now;
    List<Student> students;

    @BeforeMethod
    private void setUp(){
        studentsAnalyst = new CommonStudentsAnalyst();
        now = LocalDateTime.now();
        students = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_B, now)
        );
    }

    @Test
    public void students_average_score_of_letter_grade() {
        Optional<Integer> result = studentsAnalyst.averageScore(students);

        assertEquals(result, Optional.of(90));
    }

    @Test
    public void max_score_on_empty_list_return_option_empty() {
        Optional<Integer> result = studentsAnalyst.maxScore(Collections.emptyList());

        assertEquals(result, Optional.of(0));
    }

    @Test
    public void students_max_score_of_letter_grade() {
        Optional<Integer> result = studentsAnalyst.maxScore(students);

        assertEquals(result, Optional.of(95));
    }

    @Test
    public void min_score_on_empty_list_return_zero() {
        Optional<Integer> result = studentsAnalyst.minScore(Collections.emptyList());

        assertEquals(result, Optional.of(0));
    }

    @Test
    public void students_min_score_of_letter_grade() {
        Optional<Integer> result = studentsAnalyst.minScore(students);

        assertEquals(result, Optional.of(85));
    }

    @Test
    public void students_median_score_of_letter_grade() {
        Optional<Integer> result = studentsAnalyst.medianScore(students);

        assertEquals(result, Optional.of(95));
    }

}

