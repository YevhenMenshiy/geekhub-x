package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.exceptions.InvalidInputException;
import org.geekhub.studentregistry.grade.GradeType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.geekhub.studentregistry.grade.GradeTestData.*;
import static org.testng.Assert.*;

public class CommonStudentsFiltererTest {
    private StudentsFilterer studentsFilterer;
    private LocalDateTime now;

    @BeforeMethod
    public void setUp() {
        studentsFilterer = new CommonStudentsFilterer();
        now = LocalDateTime.now();
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void students_cant_be_filtered_for_null_input() {
        studentsFilterer.filterStudentsByGrade(GradeType.LETTER, null);
    }

    @Test
    public void filtered_students_are_not_same_array_as_empty_input_students() {
        List<Student> studentsToFilter = Collections.emptyList();

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.LETTER, studentsToFilter
        );

        assertNotSame(filteredStudents, studentsToFilter);
    }

    @Test
    public void filtered_students_are_not_same_array_as_not_empty_input_students() {
        List<Student> studentsToFilter = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.LETTER,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        assertNotSame(filteredStudents, expectedStudents);
    }

    @Test
    public void filtered_students_are_empty_if_no_students_with_required_grade_type() {
        List<Student> studentsToFilter = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.PERCENTAGE,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.emptyList();

        assertEquals(filteredStudents, expectedStudents);
    }

    @Test
    public void filtered_students_are_only_with_required_grade_type() {
        List<Student> studentsToFilter = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.LETTER,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        assertEquals(filteredStudents, expectedStudents);
    }

    @Test
    public void filtered_students_are_only_with_required_grade_type_when_input_contain_different_grade_types() {
        
        List<Student> studentsToFilter = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", PERCENTAGE_GRADE, now)
        );

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.LETTER,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        assertEquals(filteredStudents, expectedStudents);
    }

    @Test
    public void students_filtered_by_LETTER_grade_type() {
        List<Student> studentsToFilter = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", PERCENTAGE_GRADE, now)
        );

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.LETTER,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        assertEquals(filteredStudents, expectedStudents);
    }

    @Test
    public void students_filtered_by_PERCENTAGE_grade_type() {
        List<Student> studentsToFilter = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", PERCENTAGE_GRADE, now),
                new Student("name", GPA_GRADE, now)
        );

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.PERCENTAGE,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", PERCENTAGE_GRADE, now));

        assertEquals(filteredStudents, expectedStudents);
    }

    @Test
    public void students_filtered_by_GPA_grade_type() {
        List<Student> studentsToFilter = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", PERCENTAGE_GRADE, now),
                new Student("name", GPA_GRADE, now)
        );

        List<Student> filteredStudents = studentsFilterer.filterStudentsByGrade(
                GradeType.GPA,
                studentsToFilter
        );

        List<Student> expectedStudents = Collections.singletonList(new Student("name", GPA_GRADE, now));

        assertEquals(filteredStudents, expectedStudents);
    }
}