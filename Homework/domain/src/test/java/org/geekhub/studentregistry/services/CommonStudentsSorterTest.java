package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.exceptions.InvalidInputException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_A;
import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_B;
import static org.testng.Assert.*;

public class CommonStudentsSorterTest {

    private StudentsSorter studentsSorter;
    private LocalDateTime now;

    @BeforeMethod
    public void setUp(){
        studentsSorter = new CommonStudentsSorter();
        now = LocalDateTime.now();
    }

    @Test(expectedExceptions = InvalidInputException.class)
    public void students_cant_be_sorted_for_null_input() {
        studentsSorter.sortStudentsByGrade(null);
    }

    @Test
    public void sorted_students_are_not_same_array_as_empty_input_students() {
        List<Student> studentsToSort = Collections.emptyList();

        List<Student> sortedStudents = studentsSorter.sortStudentsByGrade(studentsToSort);

        assertNotSame(sortedStudents, studentsToSort);
    }

    @Test
    public void sorted_students_are_not_same_array_as_not_empty_input_students() {
        List<Student> studentsToSort = Collections.emptyList();

        List<Student> sortedStudents = studentsSorter.sortStudentsByGrade(studentsToSort);

        assertNotSame(sortedStudents, studentsToSort);
    }

    @Test
    public void students_sorted_with_only_one_student_in_input() {
        List<Student> studentsToSort = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        List<Student> sortedStudents = studentsSorter.sortStudentsByGrade(studentsToSort);

        List<Student> expectedStudents = Collections.singletonList(new Student("name", LETTER_GRADE_A, now));

        assertEquals(sortedStudents, expectedStudents);
    }

    @Test
    public void students_sorted_by_grades_with_unique_values() {
        
        List<Student> studentsToSort = Arrays.asList(
                new Student("name", LETTER_GRADE_B, now),
                new Student("name", LETTER_GRADE_A, now)
        );

        List<Student> sortedStudents = studentsSorter.sortStudentsByGrade(studentsToSort);

        List<Student> expectedStudents = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_B, now)
        );

        assertEquals(sortedStudents, expectedStudents);
    }

    @Test
    public void students_sorted_by_grades_with_same_values() {
        List<Student> studentsToSort = Arrays.asList(
                new Student("name", LETTER_GRADE_A, now),
                new Student("abc", LETTER_GRADE_A, now)
        );

        List<Student> sortedStudents = studentsSorter.sortStudentsByGrade(studentsToSort);

        List<Student> expectedStudents = Arrays.asList(
                new Student("abc", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_A, now)
        );

        assertEquals(sortedStudents, expectedStudents);
    }

}