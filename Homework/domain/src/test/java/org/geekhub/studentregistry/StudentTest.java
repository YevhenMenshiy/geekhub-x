package org.geekhub.studentregistry;

import org.geekhub.studentregistry.grade.GradeTestData;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.*;

public class StudentTest {
    private Student student;

    @BeforeMethod
    public void setUp(){
        student = new Student("name", GradeTestData.LETTER_GRADE_A, LocalDateTime.now());
    }

    @Test
    public void getNameTest(){
        assertEquals(student.getName(), "name");
    }
    @Test
    public void getGradeTest(){
        assertEquals(student.getGrade(), GradeTestData.LETTER_GRADE_A);
    }
}