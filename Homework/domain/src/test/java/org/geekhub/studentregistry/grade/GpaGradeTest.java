package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class GpaGradeTest {
    private Grade grade;

    @Test
    public void print_version_of_grade_is_4() {
        grade = new GpaGrade(100);

        String printVersion = grade.asPrintVersion();

        assertEquals(printVersion, "4.0");
    }

    @Test
    public void print_version_of_grade_is_3() {
        grade = new GpaGrade(89);

        assertEquals(grade.asPrintVersion(), "3.0");
    }

    @Test
    public void print_version_of_grade_is_2() {
        grade = new GpaGrade(79);

        assertEquals(grade.asPrintVersion(), "2.0");
    }

    @Test
    public void print_version_of_grade_is_1() {
        grade = new GpaGrade(69);

        assertEquals(grade.asPrintVersion(), "1.0");
    }

    @Test
    public void print_version_of_grade_is_0() {
        grade = new GpaGrade(59);

        assertEquals(grade.asPrintVersion(), "0.0");
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_positive_grade_value() {
        grade = new GpaGrade(101);
        grade.asPrintVersion();
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_negative_grade_value() {
        grade = new GpaGrade(-1);
        grade.asPrintVersion();
    }

    @Test
    public void testAsPrintVersion() {
        grade = new GpaGrade(100);
        assertEquals(grade.asPrintVersion(), "4.0");
    }
}