package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class PercentageGradeTest {
    private Grade grade;

    @Test
    public void print_version_of_grade_is_90_100() {
        grade = new PercentageGrade(100);

        String printVersion = grade.asPrintVersion();

        assertEquals(printVersion, "90%-100%");
    }

    @Test
    public void print_version_of_grade_is_80_89() {
        grade = new PercentageGrade(80);

        assertEquals(grade.asPrintVersion(), "80%-89%");
        grade = new PercentageGrade(89);

        assertEquals(grade.asPrintVersion(), "80%-89%");
    }

    @Test
    public void print_version_of_grade_is_70_79() {
        grade = new PercentageGrade(70);

        assertEquals(grade.asPrintVersion(), "70%-79%");
        grade = new PercentageGrade(79);

        assertEquals(grade.asPrintVersion(), "70%-79%");
    }

    @Test
    public void print_version_of_grade_is_60_69() {
        grade = new PercentageGrade(60);

        assertEquals(grade.asPrintVersion(), "60%-69%");
        grade = new PercentageGrade(69);

        assertEquals(grade.asPrintVersion(), "60%-69%");
    }

    @Test
    public void print_version_of_grade_is_less_then_60() {
        grade = new PercentageGrade(59);

        assertEquals(grade.asPrintVersion(), "<60%");
        grade = new PercentageGrade(0);

        assertEquals(grade.asPrintVersion(), "<60%");
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_positive_grade_value() {
        grade = new PercentageGrade(101);
        grade.asPrintVersion();
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_negative_grade_value() {
        grade = new PercentageGrade(-1);
        grade.asPrintVersion();
    }

    @Test
    public void asPrintVersion() {
        grade = new PercentageGrade(100);
        assertEquals(grade.asPrintVersion(), "90%-100%");
    }
}