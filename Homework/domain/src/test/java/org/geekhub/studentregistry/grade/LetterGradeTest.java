package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LetterGradeTest {

    private Grade grade;

    @Test
    public void print_version_of_grade_is_A() {
        grade = new LetterGrade(100);

        String printVersion = grade.asPrintVersion();

        assertEquals(printVersion, "A");
    }

    @Test
    public void print_version_of_grade_is_B() {
        grade = new LetterGrade(89);

        assertEquals(grade.asPrintVersion(), "B");
    }

    @Test
    public void print_version_of_grade_is_C() {
        grade = new LetterGrade(79);

        assertEquals(grade.asPrintVersion(), "C");
    }

    @Test
    public void print_version_of_grade_is_D() {
        grade = new LetterGrade(69);

        assertEquals(grade.asPrintVersion(), "D");
    }

    @Test
    public void print_version_of_grade_is_F() {
        grade = new LetterGrade(59);

        assertEquals(grade.asPrintVersion(), "F");
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_positive_grade_value() {
        grade = new LetterGrade(101);
        grade.asPrintVersion();
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_on_print_for_invalid_negative_grade_value() {
        grade = new LetterGrade(-1);
        grade.asPrintVersion();
    }

    @Test
    public void asPrintVersion(){
        grade = new LetterGrade(100);
        assertEquals(grade.asPrintVersion(), "A");
    }
}