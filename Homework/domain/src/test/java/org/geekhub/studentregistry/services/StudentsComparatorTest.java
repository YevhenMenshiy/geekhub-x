package org.geekhub.studentregistry.services;

import org.geekhub.studentregistry.Student;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_A;
import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_B;
import static org.testng.Assert.*;

public class StudentsComparatorTest {

    private StudentsComparator studentsSorter;
    private LocalDateTime now;

    @BeforeMethod
    public void setUp(){
        studentsSorter = new StudentsComparator();
        now = LocalDateTime.now();
    }

    @Test
    public void compare_first_less() {
        Student[] studentsToCompare = {
                new Student("name", LETTER_GRADE_B, now),
                new Student("name", LETTER_GRADE_A, now)
        };

        int sortedStudents = studentsSorter.compare(studentsToCompare[1], studentsToCompare[0]);

        assertEquals(sortedStudents, -1);
    }

    @Test
    public void compare_second_less() {
        Student[] studentsToCompare = {
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_B, now)
        };

        int sortedStudents = studentsSorter.compare(studentsToCompare[1], studentsToCompare[0]);

        assertEquals(sortedStudents, 1);
    }

    @Test
    public void compare_same_values_by_same_names() {
        Student[] studentsToCompare = {
                new Student("name", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_A, now)
        };

        int sortedStudents = studentsSorter.compare(studentsToCompare[0], studentsToCompare[1]);

        assertEquals(sortedStudents, 0);
    }

    @Test
    public void compare_same_values_by_diff_names() {
        Student[] studentsToCompare = {
                new Student("abc", LETTER_GRADE_A, now),
                new Student("name", LETTER_GRADE_A, now)
        };

        int sortedStudents = studentsSorter.compare(studentsToCompare[0], studentsToCompare[1]);

        assertEquals(sortedStudents, -1);
    }
}