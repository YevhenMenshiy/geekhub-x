package org.geekhub.studentregistry;

import org.geekhub.studentregistry.exceptions.InvalidStartupArgumentException;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class AppModesTest {

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void exception_if_app_modes_is_not_in_enum_for_null() {
        AppModes.from(null);
    }

    @Test(expectedExceptions = InvalidStartupArgumentException.class)
    public void exception_if_app_modes_is_not_in_enum() {
        AppModes.from("abc");
    }

    @Test
    public void AUTO_app_modes_if_letter_string_in() {
        AppModes appModes = AppModes.from("AuTo");
        assertEquals(appModes, AppModes.AUTO);
    }
    @Test
    public void MANUAL_app_modes_if_gpa_string_in() {
        AppModes appModes = AppModes.from("MANUAL");
        assertEquals(appModes, AppModes.MANUAL);
    }
}