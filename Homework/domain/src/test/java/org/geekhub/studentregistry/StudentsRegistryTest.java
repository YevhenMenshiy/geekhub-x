package org.geekhub.studentregistry;

import org.geekhub.studentregistry.io.StudentsDatabase;
import org.geekhub.studentregistry.ui.StudentsPrinter;
import org.geekhub.studentregistry.ui.StudentsReader;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.services.StudentsFilterer;
import org.geekhub.studentregistry.services.StudentsSorter;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.geekhub.studentregistry.grade.GradeTestData.LETTER_GRADE_A;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class StudentsRegistryTest {

    @Mock
    private StudentsReader studentsConsoleReader;
    @Mock
    private StudentsSorter studentsSorter;
    @Mock
    private StudentsFilterer studentsFilterer;
    @Mock
    private StudentsPrinter studentsPrinter;
    @Mock
    private StudentsRegistry studentsRegistry;
    @Mock
    private StudentsDatabase studentsDatabase;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.openMocks(this);

        studentsRegistry = new StudentsRegistry(
                studentsConsoleReader,
                studentsSorter,
                studentsFilterer,
                studentsPrinter,
                studentsDatabase
        );
    }

    @Test
    public void application_flow_is_completed_successfully() {

        List<Student> students = Collections.singletonList(new Student("name", LETTER_GRADE_A, LocalDateTime.now()));

        int totalStudentsCount = 1;

        when(studentsConsoleReader.readStudents(eq(totalStudentsCount)))
                .thenReturn(students);

        when(studentsFilterer.filterStudentsByGrade(
                any(GradeType.class),
                any(List.class)
        )).thenReturn(students);

        when(studentsSorter.sortStudentsByGrade(eq(students)))
                .thenReturn(students);

        studentsRegistry.run(totalStudentsCount);

//        verify(studentsPrinter, times(3))
//                .printStudents(any(List.class));
    }
}