package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Objects;

import static org.testng.Assert.*;

public class BaseGradeTest {
    private BaseGrade grade;

    @BeforeMethod
    public void setUp() {
        grade = new BaseGrade(100, GradeType.LETTER) {
            @Override
            public String asPrintVersion() {
                return null;
            }
        };
    }

    @Test(expectedExceptions = InvalidScoreException.class)
    public void exception_if_value_is_not_valid_less_0() {
        new BaseGrade(-1, GradeType.LETTER) {
            @Override
            public String asPrintVersion() {
                return null;
            }
        };
    }

    @Test
    public void created_letter_grade() {
        assertTrue(grade.getGradeType().equals(GradeType.LETTER));
    }

    @Test
    public void testGetValue() {
        assertEquals(grade.getValue(), 100);
    }

    @Test
    public void testGetGradeType() {
        assertEquals(grade.getGradeType(), GradeType.LETTER);
    }

    @Test
    public void testTestHashCode() {
        assertTrue(grade.hashCode() == Objects.hash(100, GradeType.LETTER));
    }

    @Test
    public void testGetGradeRangeFIRST() {
        assertEquals(grade.getGradeRange(), GradeRange.FIRST);
    }

    @Test
    public void testGetGradeRangeFIFTH() {
        grade = new BaseGrade(50, GradeType.LETTER) {
            @Override
            public String asPrintVersion() {
                return null;
            }
        };

        assertEquals(grade.getGradeRange(), GradeRange.FIFTH);
    }
}
