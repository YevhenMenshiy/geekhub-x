package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.geekhub.studentregistry.exceptions.InvalidScoreException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class CommonGradeFactoryTest {

    CommonGradeFactory commonGradeFactory;

    @BeforeMethod
    public void setUp() {
        commonGradeFactory = new CommonGradeFactory();
    }

    @Test(expectedExceptions = GradeNotSupporedException.class)
    public void cant_create_grade_instance_for_null_grade_type()  {
        commonGradeFactory.createGrade(null, 0);
    }

    @Test
    public void grade_is_created_for_grade_type_LETTER()  {
        Grade grade = commonGradeFactory.createGrade(GradeType.LETTER, 0);

        assertTrue(grade instanceof LetterGrade);
    }

    @Test
    public void grade_is_created_for_grade_type_PERCENTAGE()  {
        Grade grade = commonGradeFactory.createGrade(GradeType.PERCENTAGE, 0);

        assertTrue(grade instanceof PercentageGrade);
    }

    @Test
    public void grade_is_created_for_grade_type_GPA() {
        Grade grade = null;
        try {
            grade = commonGradeFactory.createGrade(GradeType.GPA, 0);
        } catch (InvalidScoreException e) {
            grade = commonGradeFactory.createGrade(GradeType.GPA, 0);
        }

        assertTrue(grade instanceof GpaGrade);
    }
}