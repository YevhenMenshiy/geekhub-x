package org.geekhub.studentregistry.grade;

import org.geekhub.studentregistry.exceptions.GradeNotSupporedException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class GradeTypeTest {

    GradeType gradeType;

    @AfterMethod
    public void setDown(){
        gradeType = null;
    }

    @Test(expectedExceptions = GradeNotSupporedException.class)
    public void exception_if_grade_type_is_not_in_enum_for_null() {
        GradeType.from(null);
    }

    @Test(expectedExceptions = GradeNotSupporedException.class)
    public void exception_if_grade_type_is_not_in_enum() {
        GradeType.from("abc");
    }

    @Test
    public void letter_grade_type_if_letter_string_in() {
        gradeType = GradeType.from("LettEr");
        assertEquals(gradeType, GradeType.LETTER);
    }
    @Test
    public void gpa_grade_type_if_gpa_string_in() {
        gradeType = GradeType.from("gpa");
        assertEquals(gradeType, GradeType.GPA);
    }
    @Test
    public void percentage_grade_type_if_percentage_string_in() {
        gradeType = GradeType.from("percentage");
        assertEquals(gradeType, GradeType.PERCENTAGE);
    }

    @Test
    public void percentage_grade_type_if_percentage_upper_case_string_in() {
        gradeType = GradeType.from("PERCENTAGE");
        assertEquals(gradeType, GradeType.PERCENTAGE);
    }
}