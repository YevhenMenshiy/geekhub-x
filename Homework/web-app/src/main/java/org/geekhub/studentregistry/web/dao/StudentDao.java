package org.geekhub.studentregistry.web.dao;

import org.geekhub.studentregistry.Student;

import java.util.List;
import java.util.Optional;

public interface StudentDao {
    
    int insert(Student student);

    int insertAll(List<Student> students);

    List<Student> selectAll();

    Optional<Student> selectById(int id);

    int deleteById(int id);

    int updateById(int id);

    void deleteAll();
}
