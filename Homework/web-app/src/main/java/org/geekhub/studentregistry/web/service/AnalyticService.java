package org.geekhub.studentregistry.web.service;

import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeFactory;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.web.dao.AnalyticDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class AnalyticService {

    private final Logger logger = LoggerFactory.getLogger(AnalyticService.class);
    private final AnalyticDao analyticDao;
    private final GradeFactory gradeFactory;

    @Autowired
    public AnalyticService(AnalyticDao analyticDao, GradeFactory gradeFactory) {
        this.analyticDao = analyticDao;
        this.gradeFactory = gradeFactory;
    }

    private String printAverageScore(GradeType gradeType) {
        return "Average Score " +
                analyticDao.averageScore(gradeType)
                .map(this::toAllGrades)
                .orElse("N/A");
    }

    private String printMaxScore(GradeType gradeType) {
        return "Max Score " +
                analyticDao.maxScore(gradeType)
                .map(this::toAllGrades)
                .orElse("N/A");
    }

    private String printMinScore(GradeType gradeType) {
        return "Min Score " +
                analyticDao.minScore(gradeType)
                .map(this::toAllGrades)
                .orElse("N/A");
    }

    private String printMedianScore(GradeType gradeType) {
        return "Median Score " +
                analyticDao.medianScore(gradeType)
                .map(this::toAllGrades)
                .orElse("N/A");
    }

    private String toAllGrades(int score) {
        return Stream.of(GradeType.values())
                .map(grade -> gradeFactory.createGrade(grade, score))
                .map(Grade::asPrintVersion)
                .collect(Collectors.joining(", ", "[", "]"));
    }

    public String[] getAnalytic(GradeType gradeType) {

        ExecutorService executorService = Executors.newFixedThreadPool(4);

        try {
            Future<String> maxScore = executorService.submit(() -> printMaxScore(gradeType));
            Future<String> minScore = executorService.submit(() -> printMinScore(gradeType));
            Future<String> medianScore = executorService.submit(() -> printMedianScore(gradeType));
            Future<String> averageScore = executorService.submit(() -> printAverageScore(gradeType));

            return new String[]{
                    maxScore.get(),
                    minScore.get(),
                    medianScore.get(),
                    averageScore.get(),
            };

        } catch (InterruptedException | ExecutionException e){
            Thread.currentThread().interrupt();
            logger.error(e);

        } finally {
            executorService.shutdown();
        }

        return new String[]{"Max Score N/A","Min Score N/A","Median Score N/A","Average Score N/A"};
    }
}
