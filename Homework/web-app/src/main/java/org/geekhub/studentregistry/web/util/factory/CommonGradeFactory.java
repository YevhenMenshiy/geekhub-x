package org.geekhub.studentregistry.web.util.factory;

import org.geekhub.studentregistry.grade.*;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CommonGradeFactory implements GradeFactory {

    private final Logger logger = LoggerFactory.getLogger(CommonGradeFactory.class);

    @Override
    public BaseGrade createGrade(GradeType gradeType, int value) {

        if(gradeType == null) logger.log("GradeType is not supported");

        return switch (gradeType) {
            case GPA -> new GpaGrade(value);
            case LETTER -> new LetterGrade(value);
            case PERCENTAGE -> new PercentageGrade(value);
            case UKRAINE -> new UkraineGrade(value);
        };
    }
}
