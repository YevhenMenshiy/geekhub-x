package org.geekhub.studentregistry.web.controller;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.services.CommonStudentsFilterer;
import org.geekhub.studentregistry.services.CommonStudentsSorter;
import org.geekhub.studentregistry.web.service.AnalyticService;
import org.geekhub.studentregistry.web.service.StudentRegistryService;
import org.geekhub.studentregistry.web.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/students")
@Controller
public class RegistryController {

    private static final Logger logger = LoggerFactory.getLogger(RegistryController.class);
    private final StudentService studentService;
    private final AnalyticService analyticService;
    private final StudentRegistryService studentRegistryService;

    @Autowired
    public RegistryController(StudentService studentService,
                              StudentRegistryService studentRegistryService,
                              AnalyticService analyticService) {
        this.studentService = studentService;
        this.analyticService = analyticService;
        this.studentRegistryService = studentRegistryService;
    }

    @GetMapping
    public String result(Model model,
                         CommonStudentsFilterer studentsFilterer,
                         CommonStudentsSorter studentsSorter) {

        Map<GradeType, List<Student>> studentsByGradeType = studentRegistryService.getFilteredSortedStudents(studentsFilterer, studentsSorter);
        model.addAttribute("studentsByGradeType", studentsByGradeType);
        return "all";
    }

    @GetMapping("/analytic")
    public String analytic(Model model,
                           CommonStudentsFilterer studentsFilterer,
                           CommonStudentsSorter studentsSorter){

        Map<GradeType, List<Student>> studentsByGradeType = studentRegistryService.getFilteredSortedStudents(studentsFilterer, studentsSorter);
        model.addAttribute("studentsByGradeType", studentsByGradeType);

            Map<String, String[]> analytic = new HashMap<>(Collections.emptyMap());

            for(GradeType gradeType : GradeType.values()){
                analytic.put(gradeType.name(), analyticService.getAnalytic(gradeType));
            }

            model.addAttribute("analytic", analytic);
        return "analytic";
    }

    @PostMapping("/delete-all")
    public String result(){
        studentService.deleteAll();
        return "redirect:/students";
    }
}
