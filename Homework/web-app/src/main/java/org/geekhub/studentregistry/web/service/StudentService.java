package org.geekhub.studentregistry.web.service;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.web.dao.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    private final StudentDao studentDao;

    @Autowired
    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public int addStudent(Student student){
        return studentDao.insert(student);
    }

    public int addStudents(List<Student> students){
        return studentDao.insertAll(students);
    }

    public List<Student> getAllStudents(){ return studentDao.selectAll(); }

    public Optional<Student> getStudentById(int id){
        return studentDao.selectById(id);
    }

    public int deleteStudentById(int id){
        return studentDao.deleteById(id);
    }

    public int updateStudentById(int id){
        return studentDao.updateById(id);
    }

    public void deleteAll() {
         studentDao.deleteAll();
    }
}
