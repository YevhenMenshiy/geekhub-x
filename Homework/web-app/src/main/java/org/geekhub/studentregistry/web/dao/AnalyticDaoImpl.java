package org.geekhub.studentregistry.web.dao;

import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@PropertySource("classpath:sql.properties")
public class AnalyticDaoImpl implements AnalyticDao {

    private final Logger logger = LoggerFactory.getLogger(org.geekhub.studentregistry.web.dao.StudentDaoImpl.class);
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final String sqlAverageScore;
    private final String sqlMedianScore;
    private final String sqlMaxScore;
    private final String sqlMinScore;

    @Autowired
    public AnalyticDaoImpl(NamedParameterJdbcTemplate jdbcTemplate,
                           @Value("${sql.analytic.averageScore}") String sqlAverageScore,
                           @Value("${sql.analytic.maxScore}") String sqlMaxScore,
                           @Value("${sql.analytic.minScore}") String sqlMinScore,
                           @Value("${sql.analytic.medianScore}") String sqlMedianScore
    ) {

        this.jdbcTemplate = jdbcTemplate;
        this.sqlAverageScore = sqlAverageScore;
        this.sqlMedianScore = sqlMedianScore;
        this.sqlMinScore = sqlMinScore;
        this.sqlMaxScore = sqlMaxScore;
    }

    @Override
    public Optional<Integer> averageScore(GradeType gradeType) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(
                    sqlAverageScore,
                    new MapSqlParameterSource("gradeType", gradeType.name()),
                    Integer.class
            ));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Integer> maxScore(GradeType gradeType) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(
                    sqlMaxScore,
                    new MapSqlParameterSource("gradeType", gradeType.name()),
                    Integer.class
            ));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Integer> minScore(GradeType gradeType) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(
                    sqlMinScore,
                    new MapSqlParameterSource("gradeType", gradeType.name()),
                    Integer.class
            ));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Integer> medianScore(GradeType gradeType) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(
                    sqlMedianScore,
                    new MapSqlParameterSource("gradeType", gradeType.name()),
                    Integer.class
            ));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }
}
