package org.geekhub.studentregistry.web.dao;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.*;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

@Repository
@PropertySource("classpath:sql.properties")
public class StudentDaoImpl implements StudentDao {

    private final Logger logger = LoggerFactory.getLogger(StudentDaoImpl.class);
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final String sqlStudentInsert;
    private final String sqlGradeInsert;
    private final String sqlGradeId;
    private final String sqlStudentGetAll;
    private final String sqlStudentGetById;
    private final String sqlStudentsDelete;
    private final String sqlGradeDelete;


    @Autowired
    public StudentDaoImpl(NamedParameterJdbcTemplate jdbcTemplate,
                          @Value("${sql.grade.insert}") String sqlGradeInsert,
                          @Value("${sql.grade.getId}") String sqlGradeId,
                          @Value("${sql.student.insert}") String sqlStudentInsert,
                          @Value("${sql.student.selectAll}") String sqlStudentGetAll,
                          @Value("${sql.student.selectById}") String sqlStudentGetById,
                          @Value("${sql.student.deleteAll}") String sqlStudentsDelete,
                          @Value("${sql.grade.deleteAll}") String sqlGradeDelete
                          ) {

        this.jdbcTemplate = jdbcTemplate;

        this.sqlGradeInsert = sqlGradeInsert;
        this.sqlGradeId = sqlGradeId;
        this.sqlStudentInsert = sqlStudentInsert;
        this.sqlStudentGetAll = sqlStudentGetAll;
        this.sqlStudentGetById = sqlStudentGetById;
        this.sqlStudentsDelete = sqlStudentsDelete;
        this.sqlGradeDelete = sqlGradeDelete;

    }

    @Override
    @Transactional
    public int insert(Student student) {
        SqlParameterSource toGrade = null;

        if (student.getGrade() instanceof BaseGrade) {
            BaseGrade studentGrade = (BaseGrade) student.getGrade();
            String grade_type = studentGrade.getGradeType().name();
            int value = studentGrade.getValue();

            toGrade = new MapSqlParameterSource()
                    .addValue("grade_type", grade_type)
                    .addValue("value", value);
        } else {
            logger.log("lol");
        }

        jdbcTemplate.update(sqlGradeInsert, toGrade);

        int grade_id = jdbcTemplate.queryForObject(sqlGradeId, new MapSqlParameterSource(), Integer.class);

        SqlParameterSource toStudent = new MapSqlParameterSource()
                .addValue("name", student.getName())
                .addValue("grade_id", grade_id)
                .addValue("date", student.getDate());

        jdbcTemplate.update(sqlStudentInsert, toStudent);
        return 1;
    }

    @Override
    @Transactional
    public int insertAll(List<Student> students) {

        for (Student student : students){
            insert(student);
        }

        return 1;
    }

    @Override
    public List<Student> selectAll() {
        return jdbcTemplate.query(sqlStudentGetAll, (resultSet, i) -> {

            String name = resultSet.getString("name");
            LocalDateTime date = resultSet.getObject("date", LocalDateTime.class);

            GradeType gradeType = GradeType.from(resultSet.getString("grade_type"));
            int gradeValue = resultSet.getInt("value");

            Grade grade = (new CommonGradeFactory()).createGrade(gradeType, gradeValue);

            return new Student(name, grade, date.truncatedTo(ChronoUnit.SECONDS));
        });
    }

    @Override
    public Optional<Student> selectById(int id) {
        Student student = (Student) jdbcTemplate.queryForObject(sqlStudentGetById,
                new MapSqlParameterSource("id", id),
                (resultSet, i) -> {
                    String name = resultSet.getString("name");
                    Grade grade = resultSet.getObject("grade", Grade.class);
                    LocalDateTime date = resultSet.getObject("date", LocalDateTime.class);

                    return new Student(name, grade, date.truncatedTo(ChronoUnit.SECONDS));
                });

        return Optional.ofNullable(student);
    }

    @Override
    public int deleteById(int id) {
        return 0;
    }

    @Override
    public int updateById(int id) {
        return 0;
    }

    @Override
    @Transactional
    public void deleteAll() {
        jdbcTemplate.update(sqlStudentsDelete, new MapSqlParameterSource());
        jdbcTemplate.update(sqlGradeDelete, new MapSqlParameterSource());
    }
}
