package org.geekhub.studentregistry.web.util.factory;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.CommonGradeFactory;
import org.geekhub.studentregistry.grade.Grade;
import org.geekhub.studentregistry.grade.GradeType;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class StudentFactory {

    public Optional<Student> create(String name,
                          String gradeType,
                          String gradeValue,
                          String examDate,
                          String examTime) {
        try {
            String studentName = validateName(name).orElseThrow(RuntimeException::new);
            Grade studentGrade = validateGrade(gradeType, gradeValue).orElseThrow(RuntimeException::new);
            LocalDateTime studentExamDate = validateDate(examDate, examTime).orElseThrow(RuntimeException::new);

            return Optional.of(new Student(studentName, studentGrade, studentExamDate));

        } catch (RuntimeException e) {
            return Optional.empty();
        }

    }

    private Optional<LocalDateTime> validateDate(String examDate, String examTime) {
        LocalDateTime date;

        if (examDate.isBlank() || examTime.isBlank()) return Optional.empty();

        try {
            date = LocalDateTime.parse(examDate + "T" + examTime + ":00");
        } catch (Exception e) {
            return Optional.empty();
        }

        return Optional.of(date);
    }

    private Optional<Grade> validateGrade(String gradeType, String gradeValue) {

        GradeType gt = validateGrade(gradeType).orElse(null);
        int value = validateGradeValue(gradeValue).orElse(-1);
        if (gt == null || value < 0) return Optional.empty();

        return Optional.ofNullable((new CommonGradeFactory()).createGrade(gt, value));
    }

    private Optional<GradeType> validateGrade(String gradeType) {
        return Optional.ofNullable(GradeType.from(gradeType));
    }

    private Optional<Integer> validateGradeValue(String value) {

        int gradeValue;

        if (value.isBlank()) return Optional.empty();

        try {
            gradeValue = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return Optional.empty();
        }

        if (gradeValue < 0 || gradeValue > 100) return Optional.empty();

        return Optional.of(gradeValue);
    }

    private Optional<String> validateName(String name) {
        return Optional.ofNullable(name.isBlank() ? null : name);
    }

}
