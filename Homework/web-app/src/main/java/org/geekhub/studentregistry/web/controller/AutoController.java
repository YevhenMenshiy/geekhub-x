package org.geekhub.studentregistry.web.controller;

import org.geekhub.studentregistry.ui.AutoStudentsReader;
import org.geekhub.studentregistry.web.service.StudentRegistryService;
import org.geekhub.studentregistry.web.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/auto")
@Controller
public class AutoController {

    private final StudentRegistryService studentRegistryService;

    @Autowired
    public AutoController(StudentService studentService, StudentRegistryService studentRegistryService) {
        this.studentRegistryService = studentRegistryService;
    }

    @GetMapping
    public String index(Model model) {
        return "auto/index";
    }

    @PostMapping
    public String create(Model model, @RequestParam(name = "count") String count, AutoStudentsReader asr) {

        studentRegistryService.autoGenerateStudents(count, asr);

        return "redirect:/students";
    }
}
