package org.geekhub.studentregistry.web.controller.rest;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.logger.Logger;
import org.geekhub.studentregistry.logger.LoggerFactory;
import org.geekhub.studentregistry.web.controller.RegistryController;
import org.geekhub.studentregistry.web.service.AnalyticService;
import org.geekhub.studentregistry.web.service.StudentRegistryService;
import org.geekhub.studentregistry.web.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {

    private static final Logger logger = LoggerFactory.getLogger(RegistryController.class);
    private final StudentService studentService;
    private final AnalyticService analyticService;
    private final StudentRegistryService studentRegistryService;

    @Autowired
    public StudentController(StudentService studentService,
                              StudentRegistryService studentRegistryService,
                              AnalyticService analyticService) {
        this.studentService = studentService;
        this.analyticService = analyticService;
        this.studentRegistryService = studentRegistryService;
    }

    @GetMapping()
    public List<Student> getStudent() {
        return studentService.getAllStudents();
    }

    @PostMapping()
    public ResponseEntity<?> setStudent(@RequestParam(name = "studentName") String name,
                                    @RequestParam(name = "studentGradeType") String gradeType,
                                    @RequestParam(name = "studentGradeValue") String gradeValue,
                                    @RequestParam(name = "studentExemDate") String examDate,
                                    @RequestParam(name = "studentExemTime") String examTime) {
        Optional<Student> student = studentRegistryService.createStudent(name, gradeType, gradeValue, examDate, examTime);
        return student.map(value -> new ResponseEntity<>(value, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }
}