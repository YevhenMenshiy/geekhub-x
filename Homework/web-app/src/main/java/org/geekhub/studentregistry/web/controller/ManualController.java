package org.geekhub.studentregistry.web.controller;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.ui.AutoStudentsReader;
import org.geekhub.studentregistry.web.service.StudentRegistryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@RequestMapping("/manual")
@Controller
public class ManualController {

    private final StudentRegistryService studentRegistryService;

    @Autowired
    public ManualController(StudentRegistryService studentRegistryService) {
        this.studentRegistryService = studentRegistryService;
    }

    @GetMapping
    public String index(Model model) {
        return "manual/index";
    }

    @PostMapping
    public String create(Model model,
                         @RequestParam(name = "studentName") String name,
                         @RequestParam(name = "studentGradeType") String gradeType,
                         @RequestParam(name = "studentGradeValue") String gradeValue,
                         @RequestParam(name = "studentExemDate") String examDate,
                         @RequestParam(name = "studentExemTime") String examTime
    ) {

        Optional<Student> student = studentRegistryService.createStudent(name, gradeType, gradeValue, examDate, examTime);
        model.addAttribute("message", student.isPresent() && student.get() instanceof Student ? "Student created." : "Check input data");

        if(student.isPresent() && student.get() instanceof Student)
            return "redirect:/students";
        else
            return "redirect:/manual";
    }
}
