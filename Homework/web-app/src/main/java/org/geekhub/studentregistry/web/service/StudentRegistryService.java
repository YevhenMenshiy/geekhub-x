package org.geekhub.studentregistry.web.service;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.GradeType;
import org.geekhub.studentregistry.services.CommonStudentsFilterer;
import org.geekhub.studentregistry.services.CommonStudentsSorter;
import org.geekhub.studentregistry.ui.AutoStudentsReader;
import org.geekhub.studentregistry.web.util.factory.StudentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentRegistryService {

    private final StudentService studentService;
    private final StudentFactory studentFactory;

    @Autowired
    public StudentRegistryService(StudentService studentService, StudentFactory studentFactory) {
        this.studentService = studentService;
        this.studentFactory = studentFactory;
    }

    public void autoGenerateStudents(String count, AutoStudentsReader asr) {

        int studentsCount = validateStudentCount(count);

        List<Student> students = asr.readStudents(studentsCount);
        studentService.addStudents(students);
    }

    private int validateStudentCount(String count) {
        return Integer.parseInt(count);
    }

    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    public Map<GradeType, List<Student>> getFilteredSortedStudents(CommonStudentsFilterer studentsFilterer,
                                                                   CommonStudentsSorter studentsSorter) {
        List<Student> students = getAllStudents();

        Map<GradeType, List<Student>> studentsByGradeType = new HashMap<>(Collections.emptyMap());

        for (GradeType gradeType : GradeType.values()) {
            List<Student> studentsFiltered = studentsFilterer.filterStudentsByGrade(gradeType, students);
            studentsByGradeType.put(gradeType, studentsSorter.sortStudentsByGrade(studentsFiltered));
        }

        return studentsByGradeType;
    }

    public Optional<Student> createStudent(String name, String gradeType, String gradeValue, String examDate, String examTime) {
       Optional<Student> student = studentFactory.create(name, gradeType, gradeValue, examDate, examTime);

       if(student.isEmpty()) return Optional.empty();

       studentService.addStudent(student.get());

       return student;
    }
}
