package org.geekhub.studentregistry.web.dao;

import org.geekhub.studentregistry.grade.GradeType;

import java.util.Optional;

public interface AnalyticDao {
    Optional<Integer> averageScore(GradeType gradeType);

    Optional<Integer> maxScore(GradeType gradeType);

    Optional<Integer> minScore(GradeType gradeType);

    Optional<Integer> medianScore(GradeType gradeType);
}
