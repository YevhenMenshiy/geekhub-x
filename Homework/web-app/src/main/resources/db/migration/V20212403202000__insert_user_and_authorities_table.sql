INSERT INTO users (username, password, active) VALUES('user', '$2a$10$GlpFG1Ml3U9AvkOu0D1B9ufnoquX5xqCR/NHaMfBZliYgPa8/e5sK', true);
INSERT INTO authorities (user_id, authority) VALUES(1, 'USER');
