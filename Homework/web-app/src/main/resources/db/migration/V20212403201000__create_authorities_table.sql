CREATE TABLE authorities
(
    user_id     serial NOT NULL,
    authority VARCHAR(255),

    CONSTRAINT authorities_user_id_fkey FOREIGN KEY (user_id) REFERENCES users (id)
);