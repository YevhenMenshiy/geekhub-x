CREATE TABLE grade (
id serial PRIMARY KEY,
grade_type VARCHAR(32) NOT NULL,
value INTEGER NOT NULL
);