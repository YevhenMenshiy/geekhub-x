CREATE TABLE student (
id serial PRIMARY KEY,
name VARCHAR(255) NOT NULL,
grade_id INTEGER NOT NULL,
date TIMESTAMP NOT NULL,

constraint student_grade_id_fkey foreign key (grade_id) references grade (id)
);