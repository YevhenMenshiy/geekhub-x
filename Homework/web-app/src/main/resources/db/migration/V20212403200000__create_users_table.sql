CREATE TABLE users
(
    id        serial       NOT NULL,
    username  VARCHAR(255) NOT NULL UNIQUE,
    password  VARCHAR(255) NOT NULL,
    active boolean      NOT NULL,
    PRIMARY KEY (id)
);
