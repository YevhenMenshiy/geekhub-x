package org.geekhub.studentregistry.web.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.geekhub.studentregistry.services.CommonStudentsFilterer;
import org.geekhub.studentregistry.services.CommonStudentsSorter;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.Model;

@SpringBootTest
class RegistryControllerTest {

    @Mock
    private Model modelMock;
    @Mock
    private CommonStudentsFilterer studentsFilterer;
    @Mock
    private CommonStudentsSorter studentsSorter;

    @Autowired
    private RegistryController controller;

    @Test
    public void contextLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    @Test
    public void post_result_redirect_to_student() throws Exception {
        assert(controller.result().equals("redirect:/students"));
    }

    @Test
    public void get_result() throws Exception {
        assert(controller.result(modelMock, studentsFilterer, studentsSorter).equals("all"));
    }

    @Test
    public void analytic() throws Exception {

        var result = controller.analytic(modelMock, studentsFilterer, studentsSorter);

        assert(result.equals("analytic"));
    }
}