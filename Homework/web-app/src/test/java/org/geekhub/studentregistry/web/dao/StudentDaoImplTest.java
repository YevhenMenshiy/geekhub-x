package org.geekhub.studentregistry.web.dao;

import org.geekhub.studentregistry.Student;
import org.geekhub.studentregistry.grade.LetterGrade;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
@Transactional
class StudentDaoImplTest {

    @Autowired
    StudentDaoImpl studentDao;

    @Test
    public void selectAll(){
        List<Student> students = studentDao.selectAll();
        assert(students.size() == 0);
    }

    @Test
    public void insert(){

        Student student = new Student("name", new LetterGrade(95), LocalDateTime.now());
        studentDao.insert(student);
        List<Student> students = studentDao.selectAll();

        assert(students.size() == 1);
        assert(students.get(0).getName().equals(student.getName()));
    }

    @Test
    public void deleteAll(){
        studentDao.deleteAll();
        List<Student> students = studentDao.selectAll();
        assert(students.size() == 0);
    }

}