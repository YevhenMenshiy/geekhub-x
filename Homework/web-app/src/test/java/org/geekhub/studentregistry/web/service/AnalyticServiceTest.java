package org.geekhub.studentregistry.web.service;

import org.assertj.core.api.AbstractBigDecimalAssert;
import org.geekhub.studentregistry.grade.GradeType;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest
class AnalyticServiceTest {

    @Autowired
    AnalyticService analyticService;

    @Test
    void getAnalytic() {

        String[] analytic = analyticService.getAnalytic(GradeType.UKRAINE);

        assert(Arrays.equals(analytic, new String[]{"Max Score N/A","Min Score N/A","Median Score N/A","Average Score N/A"}));
    }
}